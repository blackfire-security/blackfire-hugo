---
title: Statement of Support for CoalFire Pentesters
draft: false
layout: post
date: 2019-10-30
---

Gary DeMecurio and Justin Wynn. 

These are the names of two pentesters, two good guy hackers contracted by the state of Iowa 
to perform a physical penetration test on the Dallas County courthouse. They successfully 
gained access to the records room of the courthouse, and then... and only then... intentionally triggered 
the alarm system to measure police response time.

As one does.

They then found themselves caught in the petty politics between the county and the state, and were arrested 
and charged with felonies.

BlackFire Security employs people in this line of work, and we want to show our support for Gary and Justin. 

These charges must be dropped immediately.

This sort of scenario puts our industry in danger, and lowers the qualitative output of red team security efforts.

BlackFire Security will be drafting a letter to both Dallas County Iowa, and the State of Iowa calling for the 
immediate dismissal of charges in a show of support of our own. Physical pentesting carries risk to one's own person,
it should not include risk to the careers and lives of those who choose to sacrifice to secure our client's assets.
